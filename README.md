# ELEC5619

## Clean & Install

```
mvn clean install
```

## Run Spring

```
mvn spring-boot:run
```

## Run React

```
cd frontend
npm start
```

## Setting up the MySQL Server / Database

1) Set up a MySQL server on your own system

2) Create a user with the following credentials:

- username: ```root```
- password: ```password```

3) Access MySQL via the CLI with ```mysql -u root -p```

4) Create a database with name ```elec5619```

```sql
CREATE DATABASE elec5619;
```

5) Make sure you are in the database

```sql
USE elec5619;
```

6) Create the following tables. The tables have to match the Hibernate models in the domain directory

```sql
CREATE TABLE Event (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30)
);
```

## Populating Tables with Mock Data
http://localhost:8080/api/populate
