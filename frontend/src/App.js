import React, { useState, useEffect } from "react";
import { Dashboard } from "./ui/dashboard";
import { Login } from "./ui/login";
import { EventComponent } from "./ui/event-page";
import { Profile } from "./ui/profile";
import { Nav } from "./ui/nav";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import { UserContextProvider } from "./services/user-information";

// Styling
import { Body, Screen, SideBar, Content } from "./styled";

const Error404 = () => <h1>This page doesnt exist</h1>;

export default () => {
  const supportsHistory = "pushState" in window.history;
  const [loggedIn, setLoggedIn] = useState(true);
  const [userId, setUserId] = useState(1);

  useEffect(() => {
    // CHECK IF USER IS LOGGED IN ON SERVER
    // SET LOGGED IN TO TRUE IF THEY ARE
    /*
    const script = document.createElement("script");
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyB7Uq6PWuP7AysyJGPDvm7LWNFnp_2SdPA&libraries=places";
    script.async = true;
    document.body.appendChild(script);
    */
  }, []);

  // Temporary login/logout stuff
  const login = newUserId => {
    setLoggedIn(true);
    setUserId(newUserId);
    console.log(loggedIn);
  };

  const logout = () => {
    setLoggedIn(false);
    setUserId("");
  };

  return (
    <Body>
    <UserContextProvider userId={userId}>
      <Router forceRefresh={!supportsHistory}>
        <Switch>
          <Route path="/login">
            {loggedIn ? (
              <Redirect to="/dashboard" />
            ) : (
              <Login loginFunc={login} />
            )}
          </Route>
          <Screen>
            <SideBar>
              <Nav logout={logout} />
            </SideBar>
            <Content>
              {!loggedIn && <Redirect to="/login" />}
              <Switch>
                <Route exact path="/">
                  {loggedIn ? (
                    <Redirect to="/dashboard" />
                  ) : (
                    <Login loginFunc={login} />
                  )}
                </Route>
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/event/:id" component={EventComponent} />
                <Route path="/profile" component={Profile} />
                <Route component={Error404} />
              </Switch>
            </Content>
          </Screen>
        </Switch>
      </Router>
    </UserContextProvider>
    </Body>
  );
};


