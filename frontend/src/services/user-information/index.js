import React, { createContext, useContext, useState, useEffect } from "react";
import axios from "axios";

export const UserContext = createContext({
  id: "",
  firstName: "",
  lastName: "",
  username: ""
});
UserContext.displayName = "UserContext";

export const useUserInformation = () => useContext(UserContext);

// Provides the application with Logged in users info
export const UserContextProvider = ({ userId, children }) => {
  const [userInformation, setUserInformation] = useState({});

  useEffect(() => {
    axios.get(`http://localhost:8080/api/users/:${userId}`).then(res => {
      setUserInformation(res.data);
    });
  }, [userId]);

  return (
    <UserContext.Provider value={userInformation}>
      {children}
    </UserContext.Provider>
  );
};
