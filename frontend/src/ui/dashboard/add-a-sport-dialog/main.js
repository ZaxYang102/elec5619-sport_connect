// @flow
import React, { useState, useMemo} from "react";
import axios from "axios";
import { useUserInformation } from "../../../services/user-information";
import { tsConstructorType } from "@babel/types";

import Button from "@atlaskit/button";
import AddIcon from '@atlaskit/icon/glyph/add';
import Modal, { ModalTransition } from "@atlaskit/modal-dialog";
import { CreatableSelect } from '@atlaskit/select';

export const AddSportDialog = ({ onClick }) => {
  const [isOpen, setIsOpen] = useState("");
  const [newSport, setNewSport] = useState("");
  const { username } = useUserInformation();
  const [allSports, setAllSports] = useState([]);

  useMemo(() => {
    // get data from api 
    axios.get(`http://localhost:8080/api/sports`).then(res => { setAllSports(res.data) });
  })

  const sportOptions = allSports.map(sport => ({
    label: sport.sportName,
    value: sport.sportName
  }))

  const addSport = sport => {
    axios.all([
      axios.get(`http://localhost:8080/api/users/:${username}/sports/:${sport}`),
      axios.get(`http://localhost:8080/api/sports/:${sport}`)
    ])
      .then(() => onClick(newSport))
      .then(() => setIsOpen(false));
  };

  return (
    <span>
      <Button
        onClick={() => setIsOpen(true)}
        iconBefore={<AddIcon />} > Add a Sport </Button>
      <ModalTransition>
        {isOpen && (
          <Modal
            actions={[
              { text: "Close", onClick: () => setIsOpen(false) },
              { text: "Add Sport", onClick: () => addSport(newSport) }
            ]}
            onClose={() => setIsOpen(false)}
            heading="Add a Sport">
            <div>
              <label>
                Add a Sport:
                <CreatableSelect
                  onChange={e => setNewSport(e.value)}
                  options={sportOptions}
                />
              </label>
            </div>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
          </Modal>
        )}
      </ModalTransition>
    </span>
  );
};
