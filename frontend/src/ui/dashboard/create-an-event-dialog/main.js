// @flow
import React, { Component, useEffect } from "react";
import Button from "@atlaskit/button";

import Textfield from "@atlaskit/textfield";
import axios from "axios";
import Script from "react-load-script";
import Select from "@atlaskit/select";
import { Redirect } from "react-router-dom";

import Modal, { ModalFooter, ModalTransition } from "@atlaskit/modal-dialog";
import { DatePicker, TimePicker } from "@atlaskit/datetime-picker";

export default class CreateEventDialog extends Component {
  constructor(props) {
    super(props);

    // Bind Functions
    this.handleScriptLoad = this.handleScriptLoad.bind(this);
    this.handlePlaceSelect = this.handlePlaceSelect.bind(this);

  }

  state = {
    isOpen: false,
    name: "",
    longitude: "",
    latitude: "",
    location: "",
    sport: "",
    date: "",
    time: "",
    maxPlayers: 2,
    sportOptions: [],
    timeOptions: ['6:00', '6:30', '7:00', '7:30', '8:00', '8:30',
    '9:00', '9:30','10:00', '10:30', '11:00', '11:30', '12:00',
    '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30',
    '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30',
    '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30'],
    eventId: -1,
    redirect: false
  };

  componentDidMount() {
    axios.get(`http://localhost:8080/api/sports`).then(res => {
      this.setState({
        sportOptions: res.data.map(sport => ({
          label: sport.sportName,
          value: sport.sportName
        }))
      });
    });
  }

  open = () => this.setState({ isOpen: true });

  close = () => this.setState({ isOpen: false });

  async addUserEvent() {

    const res = await axios.get(
      `/api/events/:${this.state.name}/:${this.state.location}/:${this.state.latitude}/:${this.state.longitude}/:${this.state.sport}/:${this.state.date}/:${this.state.time}/:${this.state.maxPlayers}`
    );
    const res2 = await axios.get(
      `http://localhost:8080/api/users/:${this.props.username}/events/:${res.data.id}`
    );
    return await res.data.id;
  }

  addEvent = () => {
    (async () => {
      try {
        this.setState({ eventId: await this.addUserEvent(), redirect: true });
      } catch (e) {
        console.log(e);
      }
    })();
  };

  handleScriptLoad() {
    // Declare Options For Autocomplete
    var defaultBounds = new google.maps.LatLngBounds();
    var options = {
      types: ["address"],
      componentRestrictions: { country: "au" },
      bounds: defaultBounds
    };

    // Initialize Google Autocomplete
    /*global google*/
    this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById("autocomplete"),
      options
    );
    this.autocomplete.setFields(["address_components", "formatted_address"]);

    this.autocomplete.addListener("place_changed", this.handlePlaceSelect);
  }

  handlePlaceSelect() {
    // Extract City From Address Object
    let addressObject = this.autocomplete.getPlace();
    let address = addressObject.address_components;

    // Check if address is valid
    this.setState({
      location: addressObject.name,
    })

    axios.get(`https://us1.locationiq.com/v1/search.php?key=0b184f60c365b6&q=${this.state.location}&format=json`)
    .then(res => {
      this.setState({longitude: res.data[0].lon});
      this.setState({latitude: res.data[0].lat});
    })
}

  render() {
    const { isOpen } = this.state;
    const footer = props => (
      <ModalFooter showKeyline={props.showKeyline}>
        <span />
        <Button appearance="primary" type="submit" onClick={this.addEvent}>
          Create Event
        </Button>
      </ModalFooter>
    );

    return (
      <span>
        {this.state.eventId !== -1 && this.state.redirect === true && (
          <Redirect to={`/event/${this.state.eventId}`} />
        )}
        <Button onClick={this.open}>Create an Event</Button>

        <ModalTransition>
          {isOpen && (
            <Modal
              heading="Create an Event"
              actions={[
                { text: "Close", onClick: this.close },
                { text: "Create Event", onClick: this.addEvent }
              ]}
              onClose={this.close}
            >
              <p>Event details</p>

              <label>
                Name:
                <Textfield
                  autoComplete="off"
                  placeholder="Name"
                  value={this.state.name}
                  onChange={event => {
                    const { value } = event.target;
                    this.setState({ name: value });
                  }}
                />
                <br />
              </label>

              <label>
                Sport:
                <Select
                  placeholder="Sport"
                  options={this.state.sportOptions}
                  onChange={event => {
                    this.setState({ sport: event.label });
                  }}
                />
              </label>
              <br />
              <label>
                Location:
                <div>
                  <Script
                    url="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7Uq6PWuP7AysyJGPDvm7LWNFnp_2SdPA&libraries=places"
                    onLoad={this.handleScriptLoad}
                  />

                  <Textfield
                    id="autocomplete"
                    placeholder="Location"
                    style={{
                      margin: "0 auto",
                      maxWidth: 800
                    }}
                  />
                </div>
              </label>

              <br />

              <label>
                Date:
                <DatePicker
                  id="datepicker-1"
                  value={this.state.date}
                  dateFormat="YYYY-MM-DD"
                  placeholder="YYYY-MM-DD"
                  onChange={date => {
                    this.setState({ date: date });
                  }}
                />
              </label>

              <br />

              <label>
                Time:
                <TimePicker
                  id="timepicker-1"
                  value={this.state.time}
                  onChange={time => {
                    this.setState({ time: time });
                  }}
                  times={this.state.timeOptions}
                />
              </label>
              <br />

              <label>
                Max Players:
                <Textfield
                  type="number"
                  min="2"
                  max="100"
                  value={this.state.maxPlayers}
                  onChange={event => {
                    const { value } = event.target;
                    this.setState({ maxPlayers: value });
                  }}
                />
              </label>
              <br />
              <br />
              <br />
              <br />
              <br />
            </Modal>
          )}
        </ModalTransition>
      </span>
    );
  }
}
