// @flow
import React, { useState, useEffect } from "react";
import { AddSportDialog } from "./add-a-sport-dialog/main";
import CreateEventDialog from "./create-an-event-dialog/main";
import SportInfo from "./sport-component/main";
import {
  SportsWrapper,
  ButtonWrapper,
  Header,
  HeaderText,
  SubText
} from "./styled";
import { useUserInformation } from "../../services/user-information";
import { ButtonGroup } from "@atlaskit/button";
import axios from "axios";
import EmojiEmojiIcon from '@atlaskit/icon/glyph/emoji/emoji';


export const Dashboard = props => {
  const { username, firstName } = useUserInformation();
  const [sports, setSports] = useState([]);

  const onClick = newSport => {
    setSports([...sports, { sport: newSport }]);
  };

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/users/:${username}/sports`)
      .then(res => {
        setSports(res.data);
      });
  }, [username]);

  const deleteSport = deletedSport => {
    axios
      .get(
        `http://localhost:8080/api/users/:${username}/removeSport/:${deletedSport.sport}`
      )
      .then(setSports(sports.filter(sport => sport !== deletedSport)));
  };

  const listSports = sports.map(sport => (
    <SportInfo key={sport.sport} sport={sport} deleteSport={deleteSport} />
  ));

  return (
    <div>
      <Header>
        <HeaderText> Dashboard </HeaderText>
        <ButtonWrapper>
          <ButtonGroup>
            <AddSportDialog onClick={onClick} />
            <CreateEventDialog username={username} />
          </ButtonGroup>
        </ButtonWrapper>
      </Header>
      <Header>
        {" "}
        <SubText>
          Welcome back, {firstName}! You are following{" "}
          <u>{listSports.length}</u> sports. Click{" "} to join events near you, or create your own! <EmojiEmojiIcon size="small"/>
        </SubText>
      </Header>
      <SportsWrapper>{listSports}</SportsWrapper>
    </div>
  );
};


