// @flow
import React, { useState, useEffect } from "react";
import {
  SportContainer,
  TableWrapper,
  SportsHeader,
  FlexBox,
  CrossWrapper,
  JoinButton
} from "./styled";
import DynamicTable from "@atlaskit/dynamic-table";
import Button from "@atlaskit/button";
import axios from "axios";
import moment from "moment";
import PeopleGroupIcon from "@atlaskit/icon/glyph/people-group";
import LocationIcon from "@atlaskit/icon/glyph/location";
import CrossIcon from "@atlaskit/icon/glyph/cross";
import AddIcon from "@atlaskit/icon/glyph/add";
import EditorDividerIcon from "@atlaskit/icon/glyph/editor/divider";
import { useUserInformation } from "../../../services/user-information";
import { Link } from "react-router-dom";

const HEAD = {
  cells: [
    {
      key: "attending",
      content: <PeopleGroupIcon />,
      isSortable: true,
      width: 4
    },
    { key: "name", content: "Name", isSortable: true, width: 30, shouldTruncate: true },
    { key: "location", content: "Location", isSortable: true, shouldTruncate: true },
    { key: "time", content: "Time", isSortable: true, width: 23, shouldTruncate: true },
    { key: "distance", content: <LocationIcon />, isSortable: true, width:10 },
    { key: "join", content: "", width: 10 }
  ]
};

export default props => {
  const { username } = useUserInformation();
  const { sport, deleteSport } = props;
  const [sportData, setSportData] = useState([]);
  const [joinedEvents, setJoinedEvents] = useState([]);
  const [loadingTable, setLoadingTable] = useState(false);

  useEffect(() => {
    setLoadingTable(true);
    axios.get(`http://localhost:8080/api/events/:${sport.sport}`).then(res => {
      setSportData(res.data);
    });
    axios
      .get(`http://localhost:8080/api/users/:${username}/events/ids`)
      .then(res => {
        setJoinedEvents(res.data);
        setLoadingTable(false);
      });
  }, []);

  const leaveEvent = event => {
    axios
      .get(
        `http://localhost:8080/api/users/:${username}/removeEvent/:${event.id}`
      )
      .then(res => setJoinedEvents(res.data));
  };

  const joinEvent = event => {
    axios
      .get(`http://localhost:8080/api/users/:${username}/events/:${event.id}`)
      .then(res => {
        setJoinedEvents([...joinedEvents, res.data]);
      });
  };

  let x = 0;
  const ROWS = sportData.map(event => ({
    key: event.id,
    cells: [
      { content: (x += 1), key: x },
      { content: <Link to={`/event/${event.id}`} alt={event.name}>{event.name}</Link>, key: event.name },
      { content: event.location, key: event.location },
      { content: moment(event.start_time).format("h:mma, D/MM"), key: event.start_time },
      { content: `${x}km`, key: x },
      {
        content:
          joinedEvents.filter(userEvent => userEvent.eventId === event.id)
            .length > 0 ? (
            <JoinButton joined={true} onClick={() => leaveEvent(event)}>
              Leave
            </JoinButton>
          ) : (
            <JoinButton joined={false} onClick={() => joinEvent(event)}>
              Join
            </JoinButton>
          )
      }
    ]
  }));

  return (
    <SportContainer>
      <FlexBox>
        <SportsHeader> {sport.sport} </SportsHeader>
        <CrossWrapper>
          <Button
            iconBefore={<CrossIcon />}
            onClick={() => deleteSport(sport)}
            appearance="subtle"
          />
        </CrossWrapper>
      </FlexBox>
      <TableWrapper>
        <DynamicTable
          head={HEAD}
          rows={ROWS}
          isFixedSize={true}
          rowsPerPage={6}
          defaultPage={1}
          defaultSortKey="distance"
          defaultSortOrder="ASC"
          isLoading={loadingTable}
        />
      </TableWrapper>
    </SportContainer>
  );
};
