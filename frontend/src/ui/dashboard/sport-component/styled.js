// @flow

import styled from "styled-components";

export const SportContainer = styled.div`
  flex: 1 550px;
  min-width: 550px;
  height: 310px;
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  padding: 10px;
  margin: 10px;
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.25);
  border-radius: 3px;
  background: #fafcff;
  a {
    text-decoration: none;
    font-weight: 500;
    color: #3b5abf;
    &:hover {
      text-decoration: underline;
    }
  }
`;

export const TableWrapper = styled.div`
  margin: 5px;
  td:first-child,
  th:first-child,
  td: nth-child(5),
  th:nth-child(5),
  th:last-child,
  td:last-child {
    text-align: center;
    svg {
      height: 14px;
      width: 14px;
    }
  }
`;

export const SportsHeader = styled.div`
  font-size: 25px;
  font-weight: 500;
  dmargin: 3px;
  flex: 0 1 auto;
`;

export const FlexBox = styled.div`
  display: flex;
  justify-content: flex-start;
  position: relative;
  margin-bottom: 6px;
  padding: 5px;
`;

export const CrossWrapper = styled.div`
  flex: 0 1 auto;
  margin-left: auto;
`;

export const JoinButton = styled.button`
  width: 43px;
  text-align: center;
  padding: 0;
  background: ${props => (props.joined ? "#577eff" : "#9478ff")};
  color: ${props => (props.joined ? "white" : "white")};
  border: 0px;
  border-radius: 3px;
  cursor: pointer;
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
  &:focus {
    outline: 0;
  }
  padding-bottom: 1px;
`;
