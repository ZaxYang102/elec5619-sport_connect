// @flow

import styled from "styled-components";

export const DashboardWrapper = styled.div`
  position: relative;
  height: 100%;
`;

export const SportsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

export const ButtonWrapper = styled.div`
  align-self: flex-end;
  display: flex;
  flex-wrap: wrap;
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px 10px 10px 10px;
  padding: 10px;
  border-radius: 3px;
  background: #fafcff;
    border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.25);
`;

export const HeaderText = styled.div`
  font-size: 28px;
  font-weight: 500;
  line-height: 36px;
`;

export const SubText = styled.div`
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
`;

export const SpinnerWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
`;

