// @flow
import React, { useState } from "react";

import { useUserInformation } from "../../../../services/user-information";

import { InputContainer, Input, InputSend } from "./styled";

export const CommentAdd = props => {
  const { id } = useUserInformation();
  const [newComment, setNewComment] = useState("");

  const addComment = () => {
    props.addCommentHandler({
      // Obvs be the users Id later
      sender: id,
      text: newComment,
      created: new Date().toISOString(),
      event: props.eventId
    });
  };

  return (
    <InputContainer>
      <Input
        onChange={e => setNewComment(e.target.value)}
        placeholder="Type a message"
      ></Input>
      <InputSend onClick={addComment}>Send</InputSend>
    </InputContainer>
  );
};
