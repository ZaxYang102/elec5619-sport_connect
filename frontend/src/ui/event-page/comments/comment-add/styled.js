// @flow
import styled from "styled-components";

export const InputContainer = styled.div`
  padding: 5px;
  margin: 5px;
`;

export const Input = styled.input`
  width: 80%;
  flex: 1;
  display: inline;
  height: 32px;
  font-size: 16px;
  padding: 0 10px;
  border: none;
  background-color: rgb(245, 245, 245);
  border-radius: 6px;
  outline: none;

  &::placeholder {
    color: lightgrey;
  }

  &:hover {
    background-color: rgb(240, 240, 240);
  }

  &:focus {
    background-color: rgb(235, 235, 235);
    border: 1px solid rgb(52, 152, 219);
  }
`;

export const InputSend = styled.button`
  float: right;
  display: inline;
  height: 32px;
  background: rgb(52, 152, 219);
  color: #fafcff;
  font-size: 1em;
  padding: 0.25em 1em;
  border: 2px solid rgb(52, 152, 219);
  border-radius: 3px;
  cursor: pointer;
  transition: 0.1s all;
  outline: none;

  &:hover {
    background: rgb(41, 128, 185);
    border: 2px solid rgb(41, 128, 185);
  }

  &:focus {
    background: rgb(21, 108, 165);
    order: 2px solid rgb(21, 108, 165);
  }
`;
