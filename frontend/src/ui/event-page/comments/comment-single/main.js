// @flow
import React, { useState, useEffect } from "react";

import {
  CommentContainer,
  CommentAvatar,
  CommentText,
  CommentTimestamp,
  TextContainer
} from "./styled";

import axios from "axios";
import moment from "moment";

export const CommentSingle = props => {
  const [sender, setSender] = useState("");

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/users/:${props.comment.sender}`)
      .then(res => {
        setSender(res.data);
      });
  }, [props.comment.sender]);

  var firstName = sender.firstName;
  var lastName = sender.lastName;

  const avatarURL = `https://ui-avatars.com/api/?name=${firstName}+${lastName}`;

  return (
    <CommentContainer>
      <CommentTimestamp>
        Sent{" "}
        {moment(props.comment.created)
          .calendar()
          .toLowerCase()}
      </CommentTimestamp>
      <TextContainer>
        <CommentAvatar src={avatarURL}></CommentAvatar>
        <CommentText>{props.comment.text}</CommentText>
      </TextContainer>
    </CommentContainer>
  );
};
