// @flow
import styled from "styled-components";

export const CommentContainer = styled.div`
  // background-color: rgb(245, 245, 245);
  border-radius: 6px;
  padding: 5px;
  margin: 5px;
  margin-bottom: 20px;
  flex: 1;
`;

export const CommentAvatar = styled.img`
  border-radius: 50px;
  margin: 0;
  padding: 0;
  vertical-align: middle;
  display: inline;

  max-width: 38px;
  max-height: 38px;
  width: auto;
  height: auto;
`;

export const CommentText = styled.div`
  display: inline;
  background-color: rgb(230, 230, 230);
  border-radius: 20px;
  padding: 8px 10px;
  line-height: 22px;
  vertical-align: middle;
  margin-left: 10px;
`;

export const CommentTimestamp = styled.div`
  display: block;
  margin-bottom: 10px;
  font-size: 12px;
  color: rgb(180, 180, 180);
`;

export const TextContainer = styled.div`
  display: flex;
`;
