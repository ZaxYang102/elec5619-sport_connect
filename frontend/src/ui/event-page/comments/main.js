// @flow
import React, { useState, useEffect } from "react";

import { CommentSingle } from "./comment-single/main";
import { CommentAdd } from "./comment-add/main";

import { Container, CommentsContainer } from "./styled";

import axios from "axios";

export const CommentDisplay = props => {
  const [comments, setNewComments] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/comment/event/${props.eventId}`)
      .then(res => {
        setNewComments(res.data);
      });
  }, []);

  if (comments) {
    const listComments = comments.map(comment => (
      // key will later be equal to the comments Id
      <CommentSingle comment={comment} key={comment.text} />
    ));

    let addCommentHandler = comment => {
      setNewComments([...comments, comment]);
      scrollToBottom();

      axios.post(`http://localhost:8080/api/comment`, comment);
    };

    const scrollToBottom = () => {
      if (this.el) {
        this.el.scrollIntoView(false, { behavior: "smooth" });
      }
    };

    return (
      <Container>
        <CommentsContainer>
          {listComments}
          <div
            style={{ height: "80px" }}
            ref={el => {
              this.el = el;
            }}
          />
        </CommentsContainer>
        <CommentAdd
          addCommentHandler={addCommentHandler}
          eventId={props.eventId}
        ></CommentAdd>
      </Container>
    );
  }

  return <div></div>;
};
