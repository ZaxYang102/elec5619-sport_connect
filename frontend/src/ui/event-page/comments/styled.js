// @flow
import styled from "styled-components";

export const Container = styled.div`
  background-color: rgb(250, 250, 250);
  flex: 1;
    margin: 10px;
  padding: 10px;
  border-radius: 3px;
  background: #fafcff;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.25);
  
`;

export const CommentsContainer = styled.div`
  overflow-y: auto;
  max-height: 300px;
`;
