// @flow
import React from "react";

import { HeaderImportant, HeaderSpacer, HeaderContainer } from "./styled";

import moment from "moment";

export const EventHeader = props => {
  return (
    <HeaderContainer>
      <HeaderImportant>{props.event.name}</HeaderImportant>
      <HeaderSpacer>at</HeaderSpacer>
      <HeaderImportant>{props.event.location}</HeaderImportant>
      <HeaderSpacer>on</HeaderSpacer>
      <HeaderImportant>
        {moment(props.event.start_time).format("MMMM Do YYYY, h:mma")}
      </HeaderImportant>
    </HeaderContainer>
  );
};
