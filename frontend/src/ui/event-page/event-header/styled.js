// @flow

import styled from "styled-components";

export const HeaderImportant = styled.div`
  font-weight: 500;
  font-size: 22px;
  display: inline;
  border: 2px solid rgb(235, 235, 235);
  background-color: rgb(245, 245, 245);
  border-radius: 6px;
  padding: 5px 10px;
`;

export const HeaderSpacer = styled.div`
  font-weight: 500;
  font-size: 24px;
  display: inline;
  color: rgb(30, 30, 30);
  margin: 0 15px;
`;

export const HeaderContainer = styled.div`
  padding: 0 20px;
  text-align: center;
`;
