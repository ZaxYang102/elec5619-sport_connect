// @flow
import React, { useState, useEffect } from "react";

import { EventHeader } from "./event-header/main";
import { UserIcons } from "./user-icon/main";
import { MapDisplay } from "./map-display/main";
import { CommentDisplay } from "./comments/main";

import { EventsContainer, ItemWrapper } from "./styled";

import axios from "axios";

export const EventComponent = props => {
  const [event, setEvent] = useState({});

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/event/${props.match.params.id}`)
      .then(res => {
        setEvent(res.data);
      });
  }, [props.match.params.id]);

  const location = {
    location: event.location,
    latitude: event.latitude,
    longitude: event.longitude
  };

  return (
    <EventsContainer>
      <ItemWrapper>
        <EventHeader event={event} />
      </ItemWrapper>
      <ItemWrapper>
        <center>
          <UserIcons eventId={props.match.params.id} />
        </center>
      </ItemWrapper>
      <MapDisplay location={location} />
      <CommentDisplay eventId={props.match.params.id} />
    </EventsContainer>
  );
};
