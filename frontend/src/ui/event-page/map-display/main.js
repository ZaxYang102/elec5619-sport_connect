// @flow
import React from "react";

import { MapContainer } from "./styled";

import Map from "pigeon-maps";
import Marker from "pigeon-marker";

export const MapDisplay = props => {
  const location = props.location;

  if (location.location) {
    return (
      <MapContainer>
        <Map
          center={[location.latitude, location.longitude]}
          zoom={15}
          height={300}
        >
          <Marker
            anchor={[location.latitude, location.longitude]}
            payload={1}
            onClick={({ event, anchor, payload }) => {}}
          />
        </Map>
      </MapContainer>
    );
  } else {
    return <div></div>;
  }
};
