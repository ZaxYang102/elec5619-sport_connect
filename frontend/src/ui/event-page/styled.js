// @flow

import styled from "styled-components";

export const EventsContainer = styled.div`
  margin: 30px auto;
  width: 75%;
`;

export const ItemWrapper = styled.div`
  margin: 10px;
  padding: 10px;
  border-radius: 3px;
  background: #fafcff;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.25);
`;