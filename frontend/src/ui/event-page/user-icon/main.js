// @flow
import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useUserInformation } from "../../../services/user-information";

import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem
} from "@atlaskit/dropdown-menu";
import Button from "@atlaskit/button";

import { UserIconContainer, UserProfileImg, FlexBox, IconFlex, CrossWrapper } from "./styled";

import axios from "axios";

// converts their name to what we want
const changeName = name => {
  if (name === null || name === "") return "";
  return name.split(" ")[0];
};

export const UserIcons = props => {
  const { username } = useUserInformation();
  const [attending, setAttending] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/event/${props.eventId}/attending`)
      .then(res => {
        setAttending(
          res.data.map(username => {
            return { name: username };
          })
        );
      });
  }, []);

  const leaveEvent = eventId => {
    axios
      .get(
        `http://localhost:8080/api/users/:${username}/removeEvent/:${props.eventId}`
      ).then(setAttending(attending.filter(user => user.name !== username)));
  };

  if (attending) {
    var users = attending.map(user => {
      return {
        name: changeName(user.name),
        photo: ""
      };
    });

    // default users shown is 3
    var numToShow = Math.min(3, users.length);
    var usersShown = users.slice(0, numToShow);

    var numOthers = users.length - numToShow;
    // only show others if there are others
    if (numOthers > 0) {
      var usersHiding = users.slice(numToShow, users.length);
    }

    console.log(usersShown);

    return (
      <Fragment>
        <FlexBox>
          <IconFlex>
            {usersShown.map(user => (
              <UserIconContainer key={user.username}>
                <Link
                  to={`/profile/${user.name}`}
                  style={{ textDecoration: "none" }}
                >
                  <UserProfileImg
                    r={Math.random() * 255}
                    g={Math.random() * 255}
                    b={Math.random() * 255}
                  />
                  {user.name}
                </Link>
              </UserIconContainer>
            ))}

            {numOthers > 0 && (
              <div style={{ display: "inline-block" }}>
                <DropdownMenu
                  shouldFitContainer
                  trigger={numOthers + " more"}
                  triggerType="button"
                >
                  <DropdownItemGroup>
                    {usersHiding.map(user => {
                      return (
                        <DropdownItem key={user.username}>{user.name}</DropdownItem>
                      );
                    })}
                  </DropdownItemGroup>
                </DropdownMenu>
              </div>
            )}
          </IconFlex>
          <CrossWrapper>
            <Button onClick={() => leaveEvent()} appearance="subtle">Leave</Button>
          </CrossWrapper>
        </FlexBox>
      </Fragment>
    );
  }
  return <div></div>;
};
