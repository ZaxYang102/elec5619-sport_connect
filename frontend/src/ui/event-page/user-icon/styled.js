// @flow

import styled from "styled-components";

export const UserIconContainer = styled.div`
  display: inline-block;
  padding: 10px;
`;

export const UserProfileImg = styled.img`
  background: rgb(${props => props.r},${props => props.g},${props => props.b});
  border-radius: 50%;
  width: 30px;
  height: 30px;
  vertical-align: middle;
  padding: 5px;
`;

export const FlexBox = styled.div`
  display: flex;
  justify-content: flex-start;
  position: relative;
  margin-bottom: 6px;
  padding: 5px;
`;

export const IconFlex = styled.div`
  font-weight: 500;
  dmargin: 3px;
  flex: 0 1 auto;
`;

export const CrossWrapper = styled.div`
  flex: 0 1 auto;
  margin-left: auto;
  padding-top: 10px;
`;
