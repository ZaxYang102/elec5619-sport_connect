// @flow
import React from "react";

export const Login = ({ loginFunc }) => {
  const userId = "arozvany"
  return (
    <center>
      <h1>Login</h1>
      <h3>This really needs to be done :)</h3>
      <p>Session management needs to be managed on backend to stop defaulting to false</p>
      <p> (or by using local storage but thats cheating)</p>
      <p> Press login to access app </p>
      <p> You can also set logged in to default to true (for testing purposes) in App.js </p>
      <button onClick={() => loginFunc(userId)}>Log In </button>
    </center>
  );
};
