import React, { Fragment, useEffect, useState } from "react";
import { EventRowDiv, Small, FlexBox, EventWords, CrossWrapper } from "./styled";
import { useUserInformation } from "../../../services/user-information";

import Button from "@atlaskit/button";

import moment from "moment";
import axios from "axios";

const EventsDrawer = props => {
  const { username } = useUserInformation();
  const [events, setEvents] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/users/:${props.username}/events`)
      .then(res => {
        setEvents(
          res.data.map(event => {
            return {
              id: event[0],
              eventId: event[1],
              sport: event[2],
              location: event[3],
              startTime: event[4]
            };
          })
        );
      });
  }, []);

  const leaveEvent = event => {
    axios
      .get(
        `http://localhost:8080/api/users/:${username}/removeEvent/:${event.id}`
      );
  };

  const today = new Date();
  const upcoming = events.filter(
    event => today - new Date(event.startTime) <= 0
  );
  const past = events.filter(event => today - new Date(event.startTime) > 0);

  const EventRow = ({ event, past }) => (
    <EventRowDiv>
      <a href={`/event/${event.eventId}`}>
        <FlexBox>
          <EventWords>
              {event.sport} at {event.location}
              <Small>{moment(event.startTime).format("h:mma, MMMM Do YYYY")}</Small>
          </EventWords>
          {!past && (
            <CrossWrapper>
              <Button onClick={() => leaveEvent(event)} appearance="subtle">Leave</Button>
            </CrossWrapper>
          )}
        </FlexBox>
      </a>
    </EventRowDiv>
  );

  return (
    <Fragment>
      <h2>Upcoming Events</h2>
      {upcoming.map(e => (
        <EventRow key={e} event={e} />
      ))}
      <h2>Past Events</h2>
      {past.map(e => (
        <EventRow key={e} event={e} past />
      ))}
    </Fragment>
  );
};

export default EventsDrawer;
