import styled from "styled-components";

export const EventRowDiv = styled.div`
  padding: 10px;
  border: 1px solid rgb(220, 220, 220);
  background: rgb(240, 240, 240);
  border-radius: 5px;
  margin-bottom: 8px;
  cursor: pointer;

  &:hover {
    background: rgb(225, 225, 225);
  }

  &:last-child {
    margin-bottom: 0px;
  }

  a {
    text-decoration: none;
    color: black;
  }
`;

export const Small = styled.div`
  margin-top: 4px;
  color: grey;
  font-size: 14px;
`;

export const FlexBox = styled.div`
  display: flex;
  justify-content: flex-start;
  position: relative;
  margin-bottom: 6px;
  padding: 5px;
`;

export const EventWords = styled.div`
  font-weight: 500;
  dmargin: 3px;
  flex: 0 1 auto;
`;

export const CrossWrapper = styled.div`
  flex: 0 1 auto;
  margin-left: auto;
`;
