// @flow
import React, { useState } from "react";
import {
  NavContainer,
  NavItem,
  NavHeading,
  UserName as UserNameContainer,
  DrawerWrapper
} from "./styled";
import { useUserInformation } from "../../services/user-information";
import { Link } from "react-router-dom";

import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import EventsDrawer from "./event-drawer";
import DashboardIcon from '@atlaskit/icon/glyph/dashboard';
import mainLogo from'./real_logo.png';
import UserAvatarCircleIcon from '@atlaskit/icon/glyph/user-avatar-circle';
import PersonCircleIcon from '@atlaskit/icon/glyph/person-circle';
export const Nav = ({ logout }) => {
  const [eventDrawer, setEventDrawer] = useState(false);

  const toggleDrawer = open => event => {
    setEventDrawer(open);
  };

  const { firstName, lastName, username } = useUserInformation();
  var _username = "arozvany"; // delete this once user login is implemented

  const NavLink = ({ children, href }) => (
    <Link to={href}>
      <NavItem>
        <p>{children}</p>
      </NavItem>
    </Link>
  );

  // Astrix represent temporary links that are being used by us for now
  return (
    <NavContainer>
      <NavHeading>
        <Link to="/dashboard"><img src={mainLogo} width="100%" /> </Link>{" "}
      </NavHeading>
      <NavLink href="/dashboard"> Dashboard</NavLink>

      <NavItem onClick={toggleDrawer(true)}>
        <p>Your Events</p>
      </NavItem>
      <Drawer open={eventDrawer} onClose={toggleDrawer(false)}>
        <DrawerWrapper>
          <IconButton
            aria-label="delete"
            size="medium"
            style={{ float: "right" }}
            onClick={toggleDrawer(false)}
          >
            <ArrowLeftIcon fontSize="inherit" />
          </IconButton>
          <EventsDrawer username={_username} />
        </DrawerWrapper>
      </Drawer>

      <UserNameContainer>
        <Link to={`/profile/${username}`}>
              <PersonCircleIcon size="xlarge"/>
      <br /> <br />
          {firstName} {lastName}
        </Link>{" "}
        <br /> <br />
        <Link to="/login" onClick={() => logout()}>
          {" "}
          Logout{" "}
        </Link>
      </UserNameContainer>
    </NavContainer>
  );
};
