// @flow

import styled from "styled-components";

export const NavContainer = styled.div`
  height: 100%;
  width: 200px;
  position: fixed;
  top: 0;
  left: 0;
  background-image: linear-gradient(to bottom right, #304566, #253858);
  background: #253858;
  text-align: center;
  color: #fafcff;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  a {
    text-decoration: none;
    color: #fafcff;
  }
`;

export const NavItem = styled.div`
  width: 200px;
  display: table-cell;
  height: 65px;
  display: table;
  font-size: 1.25em;
  vertical-align: middle;
  text-decoration: none;
  text-align: center;
  cursor: pointer;

  &:hover {
    background: #304566;
    color: rgb(230, 230, 230);
  }

  &:focus {
    background: #304566;
  }

  p {
    display: table-cell;
    vertical-align: middle;
  }
`;

export const NavHeading = styled.div`
  margin: 1.5em 0 0.5em 0;
  font-size: 1.75em;
  &:hover {
    text-decoration: underline;
  }
  width: 100%;
`;

export const UserName = styled.div`
  width: 200px;
  margin-top: auto;
  margin-bottom: 20px;
  a:hover {
    text-decoration: underline;
  }
`;

export const DrawerWrapper = styled.div`
  width: 500px;
  max-width: 70vw;
  padding: 20px;
`;
