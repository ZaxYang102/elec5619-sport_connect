package au.usyd.elec5619;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAndReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAndReactApplication.class, args);
	}
}
