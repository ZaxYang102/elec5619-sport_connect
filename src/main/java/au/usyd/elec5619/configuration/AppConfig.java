package au.usyd.elec5619.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "au.usyd.elec5619")
public class AppConfig {
    
}