package au.usyd.elec5619.dao;

import au.usyd.elec5619.domain.Comment;
import au.usyd.elec5619.domain.Event;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository(value = "commentDao")
public class CommentDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveComment(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }

    public List<Comment> getAllComments() {
        Session session = getSessionFactory().openSession();

        List<Comment> commentList = session.createCriteria(Comment.class).list();

        session.close();

        return commentList;
    }

    public Comment getComment(int id) {
        Session session = getSessionFactory().openSession();

        Criteria criteria = session.createCriteria(Comment.class);
        criteria.add(Restrictions.eq("id", id));
        Comment event = (Comment) criteria.list().get(0);

        session.close();

        return event;
    }

    public List<Comment> getCommentsFromEvent(int eventId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Comment where event = :eventId");
        query.setParameter("eventId", eventId);
        return query.list();
    }
}