package au.usyd.elec5619.dao;

import javax.annotation.Resource;

import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.Event;

import java.util.List;

@Repository(value = "eventDao")
public class EventDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveEvent(Event event) {
        sessionFactory.getCurrentSession().save(event);
    }

    public List<Event> getAllEvents(){
        Session session = getSessionFactory().openSession();

        List<Event> eventList = session.createCriteria(Event.class).list();

        session.close();

        return eventList;
    }

    public Event getEvent(int id){
        Session session = getSessionFactory().openSession();

        Criteria criteria  = session.createCriteria(Event.class);
        criteria.add(Restrictions.eq("id", id));
        Event event = (Event)criteria.list().get(0);

        session.close();

        return event;
    }

    public Object getEvents(String sport) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Event where sport = :sport");
        query.setParameter("sport", sport);
        return query.list();
    }
}