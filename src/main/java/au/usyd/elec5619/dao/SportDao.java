package au.usyd.elec5619.dao;

import javax.annotation.Resource;

import org.hibernate.*;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.Sport;

import java.util.List;

@Repository(value = "sportDao")
public class SportDao {

    @Resource
    private SessionFactory sportSessionFactory;

    public SessionFactory getSessionFactory() {
        return sportSessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sportSessionFactory = sessionFactory;
    }

    public void saveSport(Sport sport) {
        sportSessionFactory.getCurrentSession().save(sport);
    }

    public Object getSport(String sportName) {
        Query query = sportSessionFactory.getCurrentSession().createQuery("from Sport where sportName = :sportName");
        query.setParameter("sportName", sportName);
        return query.list();
    }

    public Object getAllSports(){
        Query query = sportSessionFactory.getCurrentSession().createQuery("from Sport");
        return query.list();
    }

    public boolean checkSportExists(String sportName) {
        Query query = sportSessionFactory.getCurrentSession().createQuery("from Sport where sportName = :sportName");
        query.setParameter("sportName", sportName);
        if (query.list().size() != 0) {
            return true;
        }
        return false;
    }
}