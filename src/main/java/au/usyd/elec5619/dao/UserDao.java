package au.usyd.elec5619.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.User;

@Repository(value = "userDao")
public class UserDao {

    @Resource
    private SessionFactory userSessionFactory;

    public SessionFactory getSessionFactory() {
        return userSessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.userSessionFactory = sessionFactory;
    }

    public void saveUser(User user) {
        userSessionFactory.getCurrentSession().save(user);
    }

    public Object getUser(int id) {
        Query query = userSessionFactory.getCurrentSession().createQuery("from User where id = :id");
        query.setParameter("id", id);
        return query.uniqueResult();
    }
}