package au.usyd.elec5619.dao;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.UserEvent;

@Repository(value = "userEventDao")
public class UserEventDao {

    @Resource
    private SessionFactory userEventSessionFactory;

    public SessionFactory getSessionFactory() {
        return userEventSessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.userEventSessionFactory = sessionFactory;
    }

    public void saveUseEvent(UserEvent userEvent) {
        userEventSessionFactory.getCurrentSession().save(userEvent);
    }

    public Object getUserEvents(String username) {
        Query query = userEventSessionFactory.getCurrentSession()
            .createQuery("select e.id, ue.eventId, e.sport, e.location, e.startTime from Event e, UserEvent ue where e.id = ue.eventId and username = :username");
        query.setParameter("username", username);
        return query.list();
    }

    public Object getUserEventIds(String username) {
        Query query = userEventSessionFactory.getCurrentSession().createQuery("from UserEvent where username = :username");
        query.setParameter("username", username);
        return query.list();
    }


    public int removeUserEvent(String username, int eventId) {
        Query query = userEventSessionFactory.getCurrentSession()
            .createQuery("delete UserEvent where username = :username and eventId = :eventId");
        query.setParameter("username", username);
        query.setParameter("eventId", eventId);
        int result = query.executeUpdate();
        return result;
    }

    public Boolean checkIfUserAttending(String username, int eventId) {
        Query query = userEventSessionFactory.getCurrentSession()
            .createQuery("select 1 from UserEvent where username = :username and eventId = :eventId");
        query.setParameter("username", username);
        query.setParameter("eventId", eventId);
        return (query.uniqueResult() != null);
    }

    public Object getUsersAttending(int eventId) {
        Query query = userEventSessionFactory.getCurrentSession()
            .createQuery("select username from UserEvent where eventId = :eventId");
        query.setParameter("eventId", eventId);
        return query.list();
    }
}