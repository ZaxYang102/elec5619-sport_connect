package au.usyd.elec5619.dao;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.UserSport;

@Repository(value = "userSportDao")
public class UserSportDao {

    @Resource
    private SessionFactory userSportSessionFactory;

    public SessionFactory getSessionFactory() {
        return userSportSessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.userSportSessionFactory = sessionFactory;
    }

    public void saveUseSport(UserSport userSport) {
        userSportSessionFactory.getCurrentSession().save(userSport);
    }

    public boolean checkIfSportExists(String username, String sport) {
        Query query = userSportSessionFactory.getCurrentSession().createQuery("from UserSport where username = :username and sport = :sport");
       
        query.setParameter("username", username);
        query.setParameter("sport", sport);
        
        if (query.list().size() != 0) {
            return true;
        }
        return false;
    }

    public Object getUserSports(String username) {
        Query query = userSportSessionFactory.getCurrentSession().createQuery("from UserSport where username = :username");
        query.setParameter("username", username);
        return query.list();

    }

    public int removeUserSport(String username, String sport) {
        Query query = userSportSessionFactory.getCurrentSession()
                 .createQuery("delete UserSport where username = :username and sport = :sport");
        query.setParameter("username", username);
        query.setParameter("sport", sport);
        int result = query.executeUpdate();
        return result;
}
}