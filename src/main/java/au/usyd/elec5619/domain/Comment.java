package au.usyd.elec5619.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "sender")
    private int sender;

    @Column(name = "text")
    private String text;

    @Column(name = "created")
    private Date created;

    @Column(name = "event")
    private int event;

    public Comment(int sender, String text, Date created, int event) {
        this.sender = sender;
        this.text = text;
        this.created = created;
        this.event = event;
    }

    public Comment() {

    }

    // Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // Sender
    public int getSender() {
        return sender;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }

    // Text
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // Created
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    // Event
    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }
}