package au.usyd.elec5619.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name = "event")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    @Column(name = "location")
    private String location;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "maxPlayers")
    private int maxPlayers;

    @Column(name = "sport")
    private String sport;

    @Column(name = "numberAttending")
    private int numberAttending;

    public Event(String name, String location, String sport, Date startTime, Date endTime) {
        this.name = name;
        this.location = location;
        this.sport = sport;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Event(String name, String location, double latitude, double longitude, String sport, Date startTime, int maxPlayers) {
        this.name = name;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sport = sport;
        this.startTime = startTime;
        this.maxPlayers = maxPlayers;

        // only user who created the event is attending
        this.numberAttending = 1;
    }

    public Event() {

    }

    // As per design proposal, we need:

    // TODO: Users attending

    // Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // Name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Start time and date
  
    public Date getStartTime() {
        return startTime;
    }

    public void setDate(Date startTime) {
        this.startTime = startTime;
    }
    

    // End time
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    // Location
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    // Longitude
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    // Latitude
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    // Max players
    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    // Sport
    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public int getNumberAttending() {
        return numberAttending;
    }

    public void addAttendee() {
        this.numberAttending++;
    }

    public void removeAttendee() {
        this.numberAttending--;
    }

}