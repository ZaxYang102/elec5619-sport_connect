package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPORT_TABLE")
public class Sport {
    private String sportName;

    public Sport(String sportName) {
        this.sportName = sportName;
    }

    public Sport() {

    }

	@Id
    @Column(name = "SPORTNAME")
    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }
}