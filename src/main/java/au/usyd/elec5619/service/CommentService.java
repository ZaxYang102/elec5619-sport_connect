package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.CommentDao;
import au.usyd.elec5619.domain.Comment;

import java.util.List;

@Service(value = "commentService")
public class CommentService {

    @Autowired
    private CommentDao commentDao;

    public void createComment(Comment comment) {
        commentDao.saveComment(comment);
    }

    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    public Comment getComment(int id) {
        return commentDao.getComment(id);
    }

    public List<Comment> getCommentsFromEvent(int eventId) {
        return commentDao.getCommentsFromEvent(eventId);
    }
}