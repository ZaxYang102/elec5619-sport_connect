package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.EventDao;
import au.usyd.elec5619.domain.Event;

import java.util.List;

@Service(value = "eventService")
public class EventService {

    @Autowired
    private EventDao eventDao;

    public void createEvent(Event event) {
        // logic when creating an event (e.g. constraints etc)

        eventDao.saveEvent(event);
    }

    public List<Event> getAllEvents(){
        return eventDao.getAllEvents();
    }

    public Event getEvent(int id){
        return eventDao.getEvent(id);
    }

    public Object getEvents(String sport){
        return eventDao.getEvents(sport);
    }
}