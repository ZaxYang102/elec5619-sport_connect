package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.SportDao;
import au.usyd.elec5619.domain.Sport;

@Service(value = "sportService")
public class SportService {

    @Autowired
    private SportDao sportDao;

    public void createSport(Sport sport) {
        // check if sport already exists before creating a new one
        if (sportDao.checkSportExists(sport.getSportName()) == false) {
            sportDao.saveSport(sport);
        }
    }

    public Object getAllSports() {
        return sportDao.getAllSports();
    }
}