package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.UserEventDao;
import au.usyd.elec5619.domain.UserEvent;

@Service(value = "userEventService")
public class UserEventService {

    @Autowired
    private UserEventDao userEventDao;

    public Object getUserEvents(String username) {
        return userEventDao.getUserEvents(username);
    }

    public Object getUserEventIds(String username) {
        return userEventDao.getUserEventIds(username);
    }

    public void addUserEvent(UserEvent userEvent) {
        userEventDao.saveUseEvent(userEvent);
    }

    public int removeUserEvent(String username, int eventId) {
        return userEventDao.removeUserEvent(username, eventId);
    }

    public Boolean checkIfUserAttending(String username, int eventId) {
        return userEventDao.checkIfUserAttending(username, eventId);
    }

    public Object getUsersAttending(int eventId) {
        return userEventDao.getUsersAttending(eventId);
    }
}