package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.UserDao;
import au.usyd.elec5619.domain.User;

@Service(value = "userService")
public class UserService {

    @Autowired
    private UserDao userDao;

    public Object getUser(int id) {
        return userDao.getUser(id);
    }

    public void createUser(User user) {
        userDao.saveUser(user);
    }
}