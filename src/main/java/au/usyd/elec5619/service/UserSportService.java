package au.usyd.elec5619.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.UserSportDao;
import au.usyd.elec5619.domain.UserSport;

@Service(value = "userSportService")
public class UserSportService {

    @Autowired
    private UserSportDao userSportDao;

    public Object getUserSports(String username) {
        return userSportDao.getUserSports(username);
    }

    public void addUserSport(UserSport userSport) {
        userSportDao.saveUseSport(userSport);
    }

    public boolean checkIfSportExists(String username, String sport) {
        return userSportDao.checkIfSportExists(username, sport);
    }

    public int removeUserSport(String username, String sport) {
        return userSportDao.removeUserSport(username, sport);
    }
}