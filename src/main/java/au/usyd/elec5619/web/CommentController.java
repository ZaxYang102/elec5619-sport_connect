package au.usyd.elec5619.web;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.usyd.elec5619.domain.Comment;
import au.usyd.elec5619.service.CommentService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class CommentController {
    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/api/comment", method = RequestMethod.GET)
    @ResponseBody
    public List<Comment> getAllComments() {
        return commentService.getAllComments();
    }

    @RequestMapping(value = "/api/comment", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Comment createComment(@RequestBody Comment newComment) {
        commentService.createComment(newComment);
        return newComment;
    }

    @RequestMapping(value = "/api/comment/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Comment getComment(@PathVariable @NotNull int id) {
        return commentService.getComment(id);
    }

    @RequestMapping(value = "/api/comment/event/{eventId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getCommentsFromEvent(@PathVariable @NotNull int eventId) {
        return commentService.getCommentsFromEvent(eventId);
    }

    @RequestMapping(value = "/api/comment/populate", method = RequestMethod.GET)
    @ResponseBody
    public String populate() {
        Comment comment = new Comment(0, "Excited to see everyone there!", new Date(), 1);
        commentService.createComment(comment);
        comment = new Comment(1, "Don't forget your equipment", new Date(), 2);
        commentService.createComment(comment);
        comment = new Comment(1, "Should be a good time", new Date(), 3);
        commentService.createComment(comment);
        comment = new Comment(2, "Im very keen", new Date(), 1);
        commentService.createComment(comment);
        comment = new Comment(0, "Whoopee!!!!", new Date(), 2);
        commentService.createComment(comment);
        comment = new Comment(2, "Cant wait!", new Date(), 2);
        commentService.createComment(comment);
        comment = new Comment(1, "Ill be there on time for sure", new Date(), 1);
        commentService.createComment(comment);
        comment = new Comment(0, "Very excited, see you guys there", new Date(), 1);
        commentService.createComment(comment);
        return "Comment table populated";
    }
}
