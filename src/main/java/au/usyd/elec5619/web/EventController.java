package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.usyd.elec5619.domain.Event;
import au.usyd.elec5619.service.EventService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class EventController {
    @Autowired
    private EventService eventService;

    @RequestMapping(value = "/api/event", method = RequestMethod.GET)
    @ResponseBody
    public List<Event> event() {
        return eventService.getAllEvents();
    }

    @RequestMapping(value = "/api/event", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Event event(@RequestBody Event newEvent) {
        eventService.createEvent(newEvent);
        return newEvent;
    }

    @RequestMapping(value = "/api/event/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Event event(@PathVariable @NotNull int id) {
        return eventService.getEvent(id);
    }

    @RequestMapping(value = "/api/events/:{sport}", method = RequestMethod.GET)
    @ResponseBody
    public Object event(@PathVariable("sport") String sport) {
        return eventService.getEvents(sport);
    }

    // GET request to add event for TESTING purposes
    @RequestMapping(value = "/api/events/:{name}/:{location}/:{sport}", method = RequestMethod.GET)
    @ResponseBody
    public Object event(@PathVariable("name") String name, @PathVariable("location") String location,
    @PathVariable("sport") String sport) {
        Event event = new Event(name, location, sport, new Date(), new Date());
        eventService.createEvent(event);
        return event;
    }

    @RequestMapping(value = "/api/events/:{name}/:{location}/:{latitude}/:{longitude}/:{sport}/:{date}/:{time}/:{maxPlayers}", method = RequestMethod.GET)
    @ResponseBody
    public Object event(@PathVariable("name") String name, @PathVariable("location") String location,
    @PathVariable("sport") String sport, @PathVariable("latitude") String latitude,
    @PathVariable("longitude") String longitude, @PathVariable("date") String date, 
    @PathVariable("time") String time, @PathVariable("maxPlayers") int maxPlayers) throws ParseException{
        // convert java date and time to sql time stamp 
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
        
        Date tempStart = format.parse(date + "T" + time);

        Date start = new Date(tempStart.getTime() + 11 * 3600*1000);

        // convert longitude and latitude to floats 
        float floatLatitude = Float.valueOf(latitude);
        float floatLongitude = Float.valueOf(longitude);

        // only add the event if the time and date are in the future 
        if (start.compareTo(new Date()) > 0) {
            Event event = new Event(name, location, floatLatitude, floatLongitude, sport, start, maxPlayers);
            eventService.createEvent(event);
            return event;
        }
        return null;
        
    }

    @RequestMapping(value = "/api/events/populate", method = RequestMethod.GET)
    @ResponseBody
    public String populate() {
        Date now = new Date();
        
        Event event = new Event("Tennis with friends", "Victoria Park",-33.905980,151.210560, "Tennis", new Date(now.getTime() + TimeUnit.DAYS.toMillis(1)), 2);
        eventService.createEvent(event);
        event = new Event("Soccer gogo", "Prince Alfred Park",-33.910911,151.184235, "Soccer", new Date(now.getTime() + TimeUnit.DAYS.toMillis(1)), 8);
        eventService.createEvent(event);
        event = new Event("Soccer", "Prince Alfred Park",-33.900929,151.215266, "Soccer", new Date(now.getTime() + TimeUnit.DAYS.toMillis(1)), 10);
        eventService.createEvent(event);
        event = new Event("Basketballin", "Australian Technology Park",-33.895007,151.195425, "Basketball", new Date(now.getTime() + TimeUnit.DAYS.toMillis(1)), 10);
        eventService.createEvent(event);
        event = new Event("Doubles Tennis", "Alexandria Park Tennis Courts",-33.820895,151.236473, "Tennis", new Date(now.getTime() + TimeUnit.DAYS.toMillis(1)), 4);
        eventService.createEvent(event);
        return "Event table populated";
    }
}
