package au.usyd.elec5619.web;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import au.usyd.elec5619.web.EventController;
import au.usyd.elec5619.web.UserController;
import au.usyd.elec5619.web.UserEventController;
import au.usyd.elec5619.web.UserSportController;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class MockDataController {

    @Autowired
    private UserController sportController;
    @Autowired
    private UserController userController;
    @Autowired
    private EventController eventController;
    @Autowired
    private UserEventController userEventController;
    @Autowired
    private UserSportController userSportController;
    @Autowired
    private CommentController commentController;

    @RequestMapping(value = "/api/populate", method = RequestMethod.GET)
    @ResponseBody
    public String populate() {
        sportController.populate();
        userController.populate();
        eventController.populate();
        userEventController.populate();
        userSportController.populate();
        commentController.populate();
        return "Tables populated";
    }
}