package au.usyd.elec5619.web;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;


import au.usyd.elec5619.domain.Sport;
import au.usyd.elec5619.service.SportService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class SportController {
    @Autowired
    private SportService sportService;

    @RequestMapping(value = "/api/sports", method = RequestMethod.GET)
    public Object getAllSports() {
        return sportService.getAllSports();
    }

    @RequestMapping(value = "/api/sports/:{sportName}", method = RequestMethod.GET)
    public Object addNewSport(@PathVariable("sportName") String sportName) {
        Sport sport = new Sport(sportName);
        sportService.createSport(sport);
        return sport;
    }
    
    @RequestMapping(value = "/api/sports/populate", method = RequestMethod.GET)
    public String populate() {
        Sport sport = new Sport("Soccer");
        sportService.createSport(sport);
        sport = new Sport("Basketball");
        sportService.createSport(sport);
        sport = new Sport("Rugby");
        sportService.createSport(sport);
        sport = new Sport("Tennis");
        sportService.createSport(sport);
        sport = new Sport("Running");
        sportService.createSport(sport);
        sport = new Sport("Swimming");
        sportService.createSport(sport);
        sport = new Sport("Table Tennis");
        sportService.createSport(sport);
        sport = new Sport("Weight Lifting");
        sportService.createSport(sport);
        sport = new Sport("Volleyball");
        sportService.createSport(sport);
        sport = new Sport("Softball");
        sportService.createSport(sport);
        sport = new Sport("Badminton");
        sportService.createSport(sport);

        return "Sport Table Populated";
    }

}
