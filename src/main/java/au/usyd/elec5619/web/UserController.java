package au.usyd.elec5619.web;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;


import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.UserService;;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/api/users/:{id}", method = RequestMethod.GET)
    public Object createUser(@PathVariable("id") int id) {
        return userService.getUser(id);
    }

    @RequestMapping(value = "/api/users/:{username}/:{firstName}/:{lastName}/:{password}", method = RequestMethod.GET)
    public Object createUser(@PathVariable("username") String username, @PathVariable("firstName") String firstName, 
    @PathVariable("lastName") String lastName, @PathVariable("password") String password) {
        User user = new User(username, firstName, lastName, password);
        userService.createUser(user);
        return user;
    }

    @RequestMapping(value = "/api/users/populate", method = RequestMethod.GET)
    public String populate() {
        User user = new User("arozvany", "Annaliese", "Rozvany", "123456");
        userService.createUser(user);
        user = new User("rche", "Rita", "Cheung", "password");
        userService.createUser(user);
        user = new User("soccerfan1234", "The", "Lecturer", "imbadatlecuturering");
        userService.createUser(user);
        return "User Table Populated";
    }

}
