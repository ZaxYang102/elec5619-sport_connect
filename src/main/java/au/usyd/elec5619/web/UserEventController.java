package au.usyd.elec5619.web;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.usyd.elec5619.domain.UserEvent;
import au.usyd.elec5619.service.UserEventService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class UserEventController {
    @Autowired
    private UserEventService userEventService;

    @RequestMapping(value = "/api/users/:{username}/events", method = RequestMethod.GET)
    public Object getUserEvents(@PathVariable("username") String username) {
        return userEventService.getUserEvents(username);
    }

    @RequestMapping(value = "/api/users/:{username}/events/ids", method = RequestMethod.GET)
    public Object getUserEventIds(@PathVariable("username") String username) {
        return userEventService.getUserEventIds(username);
    }

    @RequestMapping(value = "/api/users/:{username}/events/:{eventId}", method = RequestMethod.GET)
    public Object createUserEvent(@PathVariable("username") String username, @PathVariable("eventId") int eventId) {
        UserEvent userEvent = new UserEvent(username, eventId);
        userEventService.addUserEvent(userEvent);
        return userEvent;
    }

    @RequestMapping(value = "/api/users/:{username}/removeEvent/:{eventId}", method = RequestMethod.GET)
    public Object removeUserEvent(@PathVariable("username") String username, @PathVariable("eventId") int eventId) {
        userEventService.removeUserEvent(username, eventId);
        return userEventService.getUserEventIds(username);
    }

    @RequestMapping(value = "/api/:{eventId}/attending/:{username}", method = RequestMethod.GET)
    public Boolean checkUserAttendingEvent(@PathVariable("username") String username, @PathVariable("eventId") int eventId) {
        Boolean result = userEventService.checkIfUserAttending(username, eventId);
        return result;
    }

    @RequestMapping(value = "/api/event/{eventId}/attending", method = RequestMethod.GET)
    public Object getUsersAttending(@PathVariable("eventId") int eventId) {
        Object obj = userEventService.getUsersAttending(eventId);
        return obj;
    }

    // populates table to have a bunch of user-eventId entries
    @RequestMapping(value = "/api/users/events/populate", method = RequestMethod.GET)
    public String populate() {
        UserEvent userEvent = new UserEvent("arozvany", 1);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("arozvany", 4);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("arozvany", 5);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("rche", 1);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("rche", 3);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("rche", 4);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("soccerfan1234", 1);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("soccerfan1234", 2);
        userEventService.addUserEvent(userEvent);
        userEvent = new UserEvent("soccerfan1234", 3);
        return "UserEvents Populated";
    }

}
