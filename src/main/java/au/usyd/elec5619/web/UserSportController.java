package au.usyd.elec5619.web;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.usyd.elec5619.domain.UserSport;
import au.usyd.elec5619.service.UserSportService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Transactional
public class UserSportController {
    @Autowired
    private UserSportService userSportService;

    @RequestMapping(value = "/api/users/:{username}/sports", method = RequestMethod.GET)
    public Object getUserSports(@PathVariable("username") String username) {
        return userSportService.getUserSports(username);
    }

    @RequestMapping(value = "/api/users/:{username}/sports/:{sport}", method = RequestMethod.GET)
    public Object createUser(@PathVariable("username") String username, @PathVariable("sport") String sport) {
        // check if user sport already exists 
        UserSport userSport = new UserSport(username, sport);
        if (userSportService.checkIfSportExists(username, sport) == false) {
            userSportService.addUserSport(userSport);
        }
        return userSport;

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/api/users/:{username}/removeSport/:{sport}", method = RequestMethod.GET)
    public Object removeUserSport(@PathVariable("username") String username, @PathVariable("sport") String sport) {
        int result = userSportService.removeUserSport(username, sport);
        return "User sport removed: " + result;
    }

    // populates table to have a bunch of user-sport entries
    @RequestMapping(value = "/api/users/sports/populate", method = RequestMethod.GET)
    public String populate() {
        UserSport userSport = new UserSport("arozvany", "Tennis");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("arozvany", "Basketball");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("arozvany", "Football");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("arozvany", "Skiing");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("rche", "Tennis");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("rche", "Basketball");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("rche", "Football");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("rche", "Skiing");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("soccerfan1234", "Tennis");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("soccerfan1234", "Basketball");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("soccerfan1234", "Football");
        userSportService.addUserSport(userSport);
        userSport = new UserSport("soccerfan1234", "Skiing");
        userSportService.addUserSport(userSport);
        return "UserSports Populated";
    }

}
